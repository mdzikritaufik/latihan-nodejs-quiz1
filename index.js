const { v4 : uuidv4} = require ('uuid');
const fs = require ('fs');
const path = require('path');

class fileManager {
    constructor(filename) {
        this.filename = filename;
        this.data = this.readFile(filename);
    }

    readFile(path) {
        const datatxt = fs.readFileSync(path, { encoding: "utf-8"})
        return JSON.parse(datatxt)
    }

    addData (newData) {
        let finaldata = this.data
        finaldata.push(newData)
        const _datastr = JSON.stringify(finaldata)
        fs.writeFileSync(this.filename , _datastr)
        return true
        
    }

    deleteData(id) {
        let finalData = this.data
        let idx = finalData.findIndex(x => x.id === id)
        
        finalData.splice(idx, 1)

       fs.writeFileSync(this.filename, JSON.stringify(finalData))
    }

    editData(id) {
        let finalData = this.data
        let idx = finalData.findIndex(x => x.id === id)

        finalData.splice(idx, 1, newData)
        fs.writeFileSync(this.filename, JSON.stringify(finalData))
    }
}

const filename = "./data.txt"
const file = new fileManager (filename)

const newData = {
    id : uuidv4(),
    nama: "Ayu",
    address: "Micronesia"
}

// method tambah data (uncomment if want to use this)
// file.addData(newData)

// method delete data (uncomment if want to use this)
// const idDelete = "987b3ac1-2e82-44b9-878f-4c1548608966"
// file.deleteData(idDelete)

//method edit data (uncomment if want to use this)
// const idEdit = '1cb3a431-cc2d-461a-a10b-110bb08ec5a0'
// file.editData(idEdit)

